# To run project
Open `dist/nested-object/index.html` in browser.

# To run dev mode
Run `npm install && ng serve`.

# Problem

Render the nested object below:

```
{
  a: true,
  b: 1,
  c: 'Hello, World!',
  d: {
    e: 'I am a nested string!',
    f: {
      g: {
        h: {
          i: 'So am I!',
          j: false,
          k: 123,
          l: 'And I am last!'
        }
      }
    }
  }
};
```

Use checkboxes for boolean, input type numbers for numbers and input type text for strings.

# Solution
1. Create a flattened structure (array) of the nested object.
2. Traverse and display array according to data type. Please see console logs to understand algorithm easier.