import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  nestedObject: any = {
    a: true,
    b: 1,
    c: 'Hello, World!',
    d: {
      e: 'I am a nested string!',
      f: {
        g: {
          h: {
            i: 'So am I!',
            j: false,
            k: 123,
            l: 'And I am last!'
          }
        }
      }
    }
  };

  flattenedObject = [];

  ngOnInit() {
    console.log('Nested object:', this.nestedObject);
    console.log('^ We need to flatten that object using flattenObject() recursive method.');
    console.log('----------------------------------------------------------------------');

    this.flattenedObject = this.flattenObject(this.nestedObject);
    console.log('This is the final flattened object:', this.flattenedObject);
  }

  // Returns flattened object given a nested object
  flattenObject(nestedObject: any, flattenedObject = []) {

    console.log(`We're now in flattenObject() method..`);
    console.log(`Let's traverse through each of nestedObject's properties.`)

    console.log('----------------------------------------------------------------------');

    Object.keys(nestedObject).forEach((key) => {

      console.log(`Now accessing property "${key}".`);

      if (typeof nestedObject[key] === 'object') {
        console.log(`Oh no! Turns out property "${key}" is an object. Let's flatten it by calling flattenObject() method.`);
        console.log('----------------------------------------------------------------------');
        return this.flattenObject(nestedObject[key], flattenedObject)
      } else {
        flattenedObject.push({
          key: key,
          value: nestedObject[key],
          type: typeof nestedObject[key],
        });
        console.log(`Great! Property "${key}" is a ${typeof nestedObject[key]} that has a value of "${nestedObject[key]}".`);
        console.log('No need to flatten. Just store it in flattenedObject variable. ');
        console.log('----------------------------------------------------------------------');
        return flattenedObject;
      }
    });

    return flattenedObject;
  }

}
